-*- markdown -*-

# About this
pyLinkantsAPI is a wrapper written in Python for the API provided by the Company Mensamatic, called LinkAnts, to send SMS messages from applications. I liked the service they provided, so I decided to give Python support to it.

# Tutorial
This tutorial is going to describe how you can send a SMS using Python in few minutes. :-)
The first thing that must be done is to have an account on Linkants service. You must enter here: [http://linkants.com/](http://linkants.com/) and create your own account. The system gives you 25 free credits for testing purposes.
Once you have that, just download linkants.py file where your project is, and just type the code:

    import linkants
    linkants.sendSMSPost('username', 'password', 'This is a Hello World SMS!!', '655444333', 'I am the sender!!', 0)


That is all!! 
If everything was OK, the number 655444333 will recieve a SMS message in seconds.

If you want more info, please refer to the Wiki section of the repository, or to the web mentioned before to see the API description and the modes of SMS messages.


# Reference
Linkants web: [http://linkants.com/](http://linkants.com/)


# Dependencies
     import pycurl
     import StringIO
     import urllib
     import hashlib as md5
     from xml.dom.minidom import parseString
