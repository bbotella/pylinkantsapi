#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
# 
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
# 
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pycurl
import StringIO
import urllib
import hashlib as md5
from xml.dom.minidom import parseString


def checkCreditsPost(account, password):
    """ checkCreditsPost is used to check the remaining credits avaliable in your account using the post method. You must provide your account info, login and password, and it returns the number of credits or -1 if something went wrong  """
    # The autentification must be done using md5 hash algorithm, so the first thing we be is to code our password
    coded_pass = md5.md5()
    coded_pass.update(password)
    coded_pass_str = coded_pass.hexdigest()
    
    # We generate a curl object, which makes a post call to Mensamatic API
    c = pycurl.Curl()
    c.setopt(pycurl.URL, "http://www.mediamovil.es/websiteSMS/sms.asmx/obtenerlimiteSMS")
    b = StringIO.StringIO()
    c.setopt(pycurl.WRITEFUNCTION, b.write)
    c.setopt(pycurl.POST, 1)
    data = {'cuenta':account, 'password':coded_pass_str}
    post = urllib.urlencode(data)
    c.setopt(pycurl.POSTFIELDS, post)
    c.perform()
    
    # Now we just have to process the xml returned by Mensamatic API
    dom = parseString(b.getvalue())
    doubleList = dom.getElementsByTagName("double")
    for double in doubleList:
        if double.childNodes[0].nodeValue == '-1':
            return -1
        else:
            return int(double.childNodes[0].nodeValue)



def sendSMSPost(account, password, body, recipient, originator, test):
    """ sendSMSPost is used to send a SMS. The data needed, following the API, is:
    account: a string with the user name
    password: a string with the password
    body: a string containing the body of the message
    recipient: a string containing the reciever of the sms
    originator: a string with the sender of the sms
    test: 0 if the sms must be sent, or 1 if the sms is just a test
    """
    # The autentification must be done using md5 hash algorithm, so the first thing we be is to code our password
    coded_pass = md5.new()
    coded_pass.update(password)
    coded_pass_str = coded_pass.hexdigest()
    
    # We generate a curl object, which makes a post call to Mensamatic API
    c = pycurl.Curl()
    c.setopt(pycurl.URL, "http://www.mediamovil.es/websiteSMS/sms.asmx/MandarSMS")
    b = StringIO.StringIO()
    c.setopt(pycurl.WRITEFUNCTION, b.write)
    c.setopt(pycurl.POST, 1)
    data = {'cuenta':account, 'password':coded_pass_str, 'body':body, 'recipient':recipient, 'originator':originator, 'test':test}
    post = urllib.urlencode(data)
    c.setopt(pycurl.POSTFIELDS, post)
    c.perform()
    
    dom = parseString(b.getvalue())
    stringList = dom.getElementsByTagName("string")
    # It depens on the string returned by the API:
    # the method returns 0 if it is in testing mode
    # return 1 if everything was OK
    # return -1 if there was any problem, and prints the error
    for str in stringList:
        if str.childNodes[0].nodeValue == 'TEST':
            return 0
        elif str.childNodes[0].nodeValue == 'OK':
            return 1
        else:
            print str.childNodes[0].nodeValue
            return -1
        
        
def sendSMSDatePost(account, password, body, recipient, originator, dt, hour, test):
    """ sendSMSDatePost is used to send a SMS on a date and time. The data needed, following the API, is:
    account: a string with the user name
    password: a string with the password
    body: a string containing the body of the message
    recipient: a string containing the reciever of the sms
    originator: a string with the sender of the sms
    dt: string containing the date in format dd-MM-YYY
    hour: string containing the exact time in which the sms must be sent, in format HH:mm:ss
    test: 0 if the sms must be sent, or 1 if the sms is just a test
    """
    # The autentification must be done using md5 hash algorithm, so the first thing we be is to code our password
    coded_pass = md5.new()
    coded_pass.update(password)
    coded_pass_str = coded_pass.hexdigest()
    
    # We generate a curl object, which makes a post call to Mensamatic API
    c = pycurl.Curl()
    c.setopt(pycurl.URL, "http://www.mediamovil.es/websiteSMS/sms.asmx/MandarSMS_FECHA")
    b = StringIO.StringIO()
    c.setopt(pycurl.WRITEFUNCTION, b.write)
    c.setopt(pycurl.POST, 1)
    data = {'cuenta':account, 'password':coded_pass_str, 'body':body, 'recipient':recipient, 'originator':originator, 'dt':dt, 'hora':hour, 'test':test}
    post = urllib.urlencode(data)
    c.setopt(pycurl.POSTFIELDS, post)
    c.perform()
    
    dom = parseString(b.getvalue())
    stringList = dom.getElementsByTagName("string")
    # It depens on the string returned by the API:
    # the method returns 0 if it is in testing mode
    # return 1 if everything was OK
    # return -1 if there was any problem, and prints the error
    for str in stringList:
        if str.childNodes[0].nodeValue == 'TEST':
            return 0
        elif str.childNodes[0].nodeValue == 'OK':
            return 1
        else:
            print str.childNodes[0].nodeValue
            return -1
        


def sendSMSVoicePost(account, password, body, recipient, originator):
    """ sendSMSVoicePost is used to send a SMS which is recorded and sent as a sound. The data needed, following the API, is:
    account: a string with the user name
    password: a string with the password
    body: a string containing the body of the message
    recipient: a string containing the reciever of the sms
    originator: a string with the sender of the sms
    """
    # The autentification must be done using md5 hash algorithm, so the first thing we be is to code our password
    coded_pass = md5.new()
    coded_pass.update(password)
    coded_pass_str = coded_pass.hexdigest()
    
    # We generate a curl object, which makes a post call to Mensamatic API
    c = pycurl.Curl()
    c.setopt(pycurl.URL, "http://www.mediamovil.es/websiteSMS/sms.asmx/MandarSMSVOZ")
    b = StringIO.StringIO()
    c.setopt(pycurl.WRITEFUNCTION, b.write)
    c.setopt(pycurl.POST, 1)
    data = {'cuenta':account, 'password':coded_pass_str, 'body':body, 'recipient':recipient, 'originator':originator}
    post = urllib.urlencode(data)
    c.setopt(pycurl.POSTFIELDS, post)
    c.perform()
    
    dom = parseString(b.getvalue())
    stringList = dom.getElementsByTagName("string")
    # It depens on the string returned by the API:
    # the method returns 0 if it is in testing mode
    # return 1 if everything was OK
    # return -1 if there was any problem, and prints the error
    for str in stringList:
        if str.childNodes[0].nodeValue == 'TEST':
            return 0
        elif str.childNodes[0].nodeValue == 'OK':
            return 1
        else:
            print str.childNodes[0].nodeValue
            return -1
        


def sendSMSCertificatedPost(account, password, body, recipient, originator, email):
    """ sendSMSCertificatedPost is used to send a SMS which is certificated. The email field is a string containing an email address where the certificate of the sms will be sent. The data needed, following the API, is:
    account: a string with the user name
    password: a string with the password
    body: a string containing the body of the message
    recipient: a string containing the reciever of the sms
    originator: a string with the sender of the sms
    email: a string containing the mail of the reciever of the certificate
    """
    # The autentification must be done using md5 hash algorithm, so the first thing we be is to code our password
    coded_pass = md5.new()
    coded_pass.update(password)
    coded_pass_str = coded_pass.hexdigest()
    
    # We generate a curl object, which makes a post call to Mensamatic API
    c = pycurl.Curl()
    c.setopt(pycurl.URL, "http://www.mediamovil.es/websiteSMS/sms.asmx/MandarSMSCERTIFICADO")
    b = StringIO.StringIO()
    c.setopt(pycurl.WRITEFUNCTION, b.write)
    c.setopt(pycurl.POST, 1)
    data = {'cuenta':account, 'password':coded_pass_str, 'body':body, 'recipient':recipient, 'originator':originator, 'email':email}
    post = urllib.urlencode(data)
    c.setopt(pycurl.POSTFIELDS, post)
    c.perform()
    
    dom = parseString(b.getvalue())
    stringList = dom.getElementsByTagName("string")
    # It depens on the string returned by the API:
    # the method returns 0 if it is in testing mode
    # return 1 if everything was OK
    # return -1 if there was any problem, and prints the error
    for str in stringList:
        if str.childNodes[0].nodeValue == 'TEST':
            return 0
        elif str.childNodes[0].nodeValue == 'OK':
            return 1
        else:
            print str.childNodes[0].nodeValue
            return -1
        

def sendSMSContractPost(account, password, body, recipient, originator, email):
    """ sendSMSContractPost is used to send a SMS which has the legal aspects of a contract. The email field is a string containing an email address where the contract and info of the sms will be sent. The data needed, following the API, is:
    account: a string with the user name
    password: a string with the password
    body: a string containing the body of the message
    recipient: a string containing the reciever of the sms
    originator: a string with the sender of the sms
    email: a string containing the mail of the reciever of the certificate
    """
    # The autentification must be done using md5 hash algorithm, so the first thing we be is to code our password
    coded_pass = md5.new()
    coded_pass.update(password)
    coded_pass_str = coded_pass.hexdigest()
    
    # We generate a curl object, which makes a post call to Mensamatic API
    c = pycurl.Curl()
    c.setopt(pycurl.URL, "http://www.mediamovil.es/websiteSMS/sms.asmx/MandarSMSCONTRATO")
    b = StringIO.StringIO()
    c.setopt(pycurl.WRITEFUNCTION, b.write)
    c.setopt(pycurl.POST, 1)
    data = {'cuenta':account, 'password':coded_pass_str, 'body':body, 'recipient':recipient, 'originator':originator, 'email':email}
    post = urllib.urlencode(data)
    c.setopt(pycurl.POSTFIELDS, post)
    c.perform()
    
    dom = parseString(b.getvalue())
    stringList = dom.getElementsByTagName("string")
    # It depens on the string returned by the API:
    # the method returns 0 if it is in testing mode
    # return 1 if everything was OK
    # return -1 if there was any problem, and prints the error
    for str in stringList:
        if str.childNodes[0].nodeValue == 'TEST':
            return 0
        elif str.childNodes[0].nodeValue == 'OK':
            return 1
        else:
            print str.childNodes[0].nodeValue
            return -1